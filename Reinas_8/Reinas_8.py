# David Vargas Q
# IA
# Profesor: 

#Variables Globales

# Lista de pares ordenados colocados en la matriz q tienen una reina
ListaParesColocados = [[3,4],[6,0]]

M = []

M = [[0 for i in range(8)] for j in range(8)]

M[6][7] = 1

print (M)

# Validar Filas
# Retorna True si en esa fila se puede colocar una reina, sino False
# Parametros: numero de fila
def validarFila(f):
    c = 0
    if (f > 7):
        print ("Numero de fila debe ser menor a 8")
        return False
    while (c < 8):
        if (M[f][c] == 1):
            return False
        c += 1
    return True

# Validar Columnas
# Retorna True si en esa columna se puede colocar una reina, sino False
# Parametros: numero de columna
def validarColumna(c):
    con = 0
    if (c > 7):
        print ("Numero de columna debe ser menor a 8")
        return False
    while (con < 8):
        if (M[con][c] == 1):
            return False
        con += 1
    return True

# Recibe dos puntos de la matriz: (x1,x2) posicion de una dama en el tablero, al igual que (y1,y2)
# Retorna: True si las damas estan en diferentes diaganales , sino False
def mismaDiagonal(x1,x2,y1,y2):
    if(abs(x1-y1) - abs(x2-y2) == 0):
        return False
    return True

# Validar Diagonales
# Retorna True si en las diagonales con respecto a un punto se puede colocar una reina, sino False
# Parametros: fila, columna
def validarDiagonales(x1,x2):
    contador = 0
    while(contador < len(ListaParesColocados)):
        a = ListaParesColocados[contador][0]
        b = ListaParesColocados[contador][1]
        r = mismaDiagonal(x1,x2,a,b)
        if(r == False):
            return r
        contador += 1

    return True

